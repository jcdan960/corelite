#ifndef PROCESSMANAGER_H
#define PROCESSMANAGER_H

#include <json.hpp>
#include "MainThread.h"

class ProcessManager {
public:
	ProcessManager(std::string const & path);
	void StartSystem();
	void StopSystem();


	bool Boot();
	bool LoadConfig();

public:
	std::string _configPath;
	std::unique_ptr<MainThread> _mainThread;
	std::shared_ptr<std::unordered_map <std::string, json::JSON>> _config;
};

#endif // !PROCESSMANAGER_H