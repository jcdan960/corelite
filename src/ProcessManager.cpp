#include <include/ProcessManager.h>
#include <include/SysLog.h>
#include <memory>

#if defined(WIN32)|| defined(MINGW32) || defined(MINGW6)
#include <Windows.h>

#else
#include <csignal>
#include <unistd.h>
#include <stdlib.h>

#endif

ProcessManager::ProcessManager(std::string const &path) :
	_configPath(path),
	_config(nullptr){
    SysLog::Init();
}

void ProcessManager::StartSystem() {
	if (Boot())
		SysLog::Log(SysLog::SeverityLevel::Notice, "Boot successful");
	else {
		SysLog::Log(SysLog::SeverityLevel::Alert,"Failure booting");
		return;
	}

	_mainThread->Run();
}

void ProcessManager::StopSystem() {
	SysLog::Log(SysLog::SeverityLevel::Notice, "Stopping system");
}


bool ProcessManager::Boot() {

	SysLog::Log(SysLog::SeverityLevel::Notice, "System Booting...");

#if defined(WIN32)|| defined(MINGW32) || defined(MINGW6)

	SysLog::Log(SysLog::SeverityLevel::Informational,"Windows platform detected");

#else

	SysLog::Log(SysLog::SeverityLevel::Informational,"Linux platform detected");

#endif

	SysLog::Log(SysLog::SeverityLevel::Notice, "Loading Config...");

	bool successBoot = LoadConfig();

	if (!successBoot)
		return false;

	SysLog::Log(SysLog::SeverityLevel::Notice, "Config Loaded");
	_mainThread = std::make_unique<MainThread>(_config);

	return true;
}

bool ProcessManager::LoadConfig() {
	json::JSON configFile = json::JSON::LoadFromFile(_configPath);

	auto cfg = std::make_shared<json::JSON>(configFile["Modules"]);

	if (cfg->length() < 0)
		return false;

	_config = std::make_shared<std::unordered_map<std::string, json::JSON>>();

	for (uint16_t i = 0; i < cfg->length(); ++i){
		auto json = cfg->at(i);

		_config->insert({json["name"].ToString() , json});
	}

	return true;	
}